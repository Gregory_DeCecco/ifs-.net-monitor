**IFS Apps9 .NET Monitor**

**Background**

IFS wrote an application that monitors the health of their Apps9 application.  
The source code was given to Logicalis with the goal of integrating it with Nimsoft and ServiceNow.  


**What It Does**

This code installs as a service on a Windows OS.
It monitors three aspects of an Apps9 install:
1. Is the Oracle Database service responding to queries?
2. Is the web server responding?
3. Is the Apps9 web service responding?

The results of these three queries are written to a log file.  
This log file is parsed by Nimsoft.  
When errors are found, Nimsoft opens a ticket in ServiceNow.


**How To Edit**

Use Visual Studio 2015 Community Edition to view this code.


**More Information:**

Installation and usage notes can be found in the ServiceNow article [KB0021490](https://optimal.service-now.com/nav_to.do?uri=%2Fkb_view_customer.do%3Fsys_kb_id%3D2b00220e3716f6c8cf13616043990e09%26sysparm_nameofstack%3D%26sysparm_kb_search_table%3D).