﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;

namespace IFSApplicationMonitor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 

        // Initialize Log4Net Logger
        // https://csharp.today/log4net-tutorial-great-library-for-logging/
        // https://www.codeproject.com/articles/140911/log-net-tutorial
        private static readonly log4net.ILog Log4NetLogger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //private static string LogFileName       = "";
        private static string AppenderName      = "RollingFileAppender";  // <-- This references the name of an appender in the app.config file
        //private static string DebugLogFileName  = "";
        private static string DebugAppenderName = "RollingFileAppenderDebug";  // <-- This references the name of an appender in the app.config file

        static void Main()
      {
            try
            {
                // Guaranteed logging to the system/user temp directory
                //if (SetLog4netLogLocation(AppenderName, Path.GetTempPath()) == false) { throw new Exception("Log File Directory '" + Path.GetTempPath() + "' is invalid."); }
                //if (SetLog4netLogLocation(DebugAppenderName, Path.GetTempPath()) == false) { throw new Exception("Log File Directory '" + Path.GetTempPath() + "' is invalid."); }

                Log4NetLogger.Info("==============================================");
                Log4NetLogger.Info("==============================================");
                Log4NetLogger.Info("The IFS Apps9 Monitor application has started.");

                // Try to parse the log path from the user-configurable config file
                if (SetLog4netLogLocation(AppenderName, Properties.Settings.Default.LogFileDirectory) == false) { throw new Exception("Log File Directory '" + Properties.Settings.Default.LogFileDirectory + "' is invalid."); }
                if (SetLog4netLogLocation(DebugAppenderName, Properties.Settings.Default.LogFileDirectory) == false) { throw new Exception("Log File Directory '" + Properties.Settings.Default.LogFileDirectory + "' is invalid."); }
            }
            catch (Exception e)
            {
                Log4NetLogger.Error("An error occurred while reading parameter 'LogFileDirectory' from the configuration file.  Error Message: " + e.Message.ToString());
                Log4NetLogger.Info("Application will now terminate.");
                System.Environment.Exit(1);
            }

#if (!DEBUG)
            // This will execute when installed as a service

            Log4NetLogger.Info("Running application in service mode.");
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new Service1() 
			};
            ServiceBase.Run(ServicesToRun);
#else
            // This will execute during debugging inside Visual Studio
            Log4NetLogger.Info("Running application in debug mode.");
            Service1 myService1 = new Service1();

            // When .StartMonitoring() is called, it will stay there and run until an error happens
            myService1.StartMonitoring();

            // An error happened therefore the service should stop
            myService1.Stop();
#endif
        }

        private static bool SetLog4netLogLocation(string AppenderName, string newLogDiretory)
        {
            // Return true if the appender exists
            // Return false if the appender does not exist
            // Return false if the new directory is invalid

            // Verify the proposed location exists
            if (Directory.Exists(newLogDiretory))
            {
                // Find and update the RollingFileAppender
                foreach (var appender in log4net.LogManager.GetRepository().GetAppenders())
                {
                    if (appender.Name == AppenderName)
                    {
                        // Cast Appender to generic 'FileAppender' type
                        log4net.Appender.FileAppender logger = (log4net.Appender.FileAppender)appender;

                        string oldPath = logger.File;

                        string newPath = Path.Combine(newLogDiretory, Path.GetFileName(oldPath));

                        if (oldPath != newPath)
                        {
                            Log4NetLogger.Info("Logging path for '" + AppenderName + "' is being changed from '" + oldPath + "' to '" + newPath + "'.");

                            logger.File = newPath;
                            logger.ActivateOptions();

                            Log4NetLogger.Info("Logging path for '" + AppenderName + "' has changed from '" + oldPath + "' to '" + newPath + "'.");
                        }
                        return true;  //success
                    }
                }
            }
            else
            {
                Log4NetLogger.Info("Logging path change request to '" + newLogDiretory + "'.  This is an invalid path. Active logging file has not changed.");
            }

            return false;
        }
    }
}
