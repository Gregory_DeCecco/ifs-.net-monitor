﻿using System;
using System.Collections.Generic;
using System.Text;
using Ifs.Fnd.AccessProvider;
using Ifs.Fnd.AccessProvider.PLSQL;
using Ifs.Fnd.AccessProvider.Interactive;

namespace IFSApplicationMonitor
{
    public class ExtConnection
    {
        private FndConnection connection;
        public string userID = "";

        public FndPLSQLCommand GetFndPLSQLCommand(string command)
        {
            return new FndPLSQLCommand(this.connection, command);
        }

        public FndPLSQLSelectCommand GetFndPLSQLSelectCommand(string command)
        {
            return new FndPLSQLSelectCommand(this.connection, command);
        }

        public bool IsLoggedExtServer(string conectionString, string username, string password)
        {
            if (this.connection == null)
            {
                return this.LoginExtServer(conectionString, username, password);
            }
            return this.connection.IsSessionInitialized;
        }

        public bool LoginExtServer(string conectionString, string username, string password)
        {
            this.connection = new FndConnection();
            FndLoginDialog dialog = new FndLoginDialog(this.connection);
            FndLoginCredentials defaultCredentials = new FndLoginCredentials
            {
                ConnectTo = conectionString,
                //IdentifiedExternally = true,
                //ShowLoginDialog = false,
                UserName = username,
                Password = password
            };
            dialog.ShowDialog(defaultCredentials, true, false);
            return this.connection.IsSessionInitialized;
        }

        internal void LogoutExtServer(string conectionString, string username, string password)
        {
            if (this.IsLoggedExtServer(conectionString, username, password))
            {
                this.connection.CloseDedicatedSession();
            }
        }

        public FndConnection Connection
        {
            get
            {
                return this.connection;
            }
            set
            {
                this.connection = value;
            }
        }
    }
}
