﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Collections.Specialized;
using Oracle.ManagedDataAccess.Client;
using System.Configuration;
using System.IO;
using System.Threading;
using System.Net.Mail;
//using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Security;

namespace IFSApplicationMonitor
{
    public class Monitor
    {
        #region Variables

        private struct ServerSettings
        {
            public string dbHost;
            public string dbPort;
            public string dbSID;
            public string dbUser;
            public SecureString dbPasswd;
            public int dbSupressionDuration;
            public string appOwner;
        }
        private struct ApplicationServerSettings
        {
            public string url;
            public string username;
            public SecureString password;
            public string soapgateway;
        }
        private struct MailSettings
        {
            public string mailFrom;
            public string mailTo;
            public string mailSubject;
            public string mailServer;
            public int    mailServerPort;
            public bool   smtpAuth;
            public string smtpUsername;
            public string smtpPassword;
        }

        MailSettings emailSettings = new MailSettings();
        string Log4NetLogName      = "RollingFileAppender";
        string Log4NetDebugLogName = "RollingFileAppenderDebug";
        string Log4NetNimsoftLogName = "RollingFileAppenderNimsoft";
        string customerName = "";
        int intPollEveryXseconds = 60;
        bool MonitorStatus = true; // True = Healthy.  False = Unhealthy. 

        // Initialize Log4Net Logger
        // https://csharp.today/log4net-tutorial-great-library-for-logging/
        // https://www.codeproject.com/articles/140911/log-net-tutorial
        // http://stackoverflow.com/questions/17106559/using-log4net-to-write-to-different-loggers
        private static readonly log4net.ILog Log4NetLogger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly log4net.ILog Log4NetLoggerNimsoft = log4net.LogManager.GetLogger("NimsoftLog");
        #endregion

        public Monitor()
        {
            // Default Constructor
        }

        public void Poll()
        {
            string NimsoftLogTemplate = "<Server_Name>,<Instance_Name>,<Test_Name>,<Test_Result>,<Message>";
            string ErrorMessage = "";

            Log4NetLogger.Info("========== Starting Polling Session ==========");

            // Update running config
            // Stuff like Customer name, and email alert settings
            // Returns True if successful, False if errors
            if (ReadConfig()) {
                try
                {
                    // Each server to monitor is registered in 'AppSettings'

                    // Reload the settings
                    ConfigurationManager.RefreshSection("appSettings");
                    NameValueCollection appSetting = ConfigurationManager.AppSettings;
                    
                    // Loop through each registered server (AppSetting Key)
                    for (int i = 0; i < appSetting.Count; i++)
                    {
                        // Parameters are stored in CSV format.  Extract them.
                        string[] keys = appSetting.GetKey(i).Split(',');

                        /*  Key CSV string breakdown
                         *  ========================
                         *  
                         *  Elements:     0          1    2     3          4          5        6 7                 TRUE or FALSE
                         *  =========     ========== ==== ===== ========== ========== ======== = ================  ================
                         *  <appSettings>
                         *      <add key="ServerName,Port,dbSID,dbUserName,dbPassword,appOwner,0,dbPasswordSecure" value="Enabled?"/>
                         *  </appSettings>
                         */

                        // Skip the keys that are instructions/examples
                        if (keys[0].Contains("ServerName") || keys[0].Contains("ExampleServer") )
                        {
                            // Skip this key without making an entry in any log file
                            continue;
                        }
                        
                        // Should this registered server be polled or skipped?
                        bool bActive = Convert.ToBoolean(appSetting.GetValues(i)[0].ToString());

                        Log4NetLogger.Info("----------------------------------------------");

                        
                        // Does this CSV string include an encrypted password or plaintext password?
                        if (keys[4].ToString().Trim().Length > 0)
                        {
                            // A plaintext password exists
                            // Encrypt it into a SecureString and rebuild the key
                            // Overwrite an pre-existing SecureString
                            keys[7] = EncryptString(ToSecureString(keys[4].ToString().Trim()));

                            // Blank the old plaintext password
                            keys[4] = null;

                            // Build the new key
                            string newKey = String.Join(",", keys);

                            // Add the new key to AppSettings
                            // ConfigurationManager.AppSettings.Add(newKey, appSetting.GetValues(i)[0]);  <-- AppSettings is always read-only

                            /* You can't update the configuration using ConfigurationSettings or ConfigurationManager at runtime.  
                             * You can add values because the AppSettings property is just a name/value collection.  
                             * App.config is usually considered read-only at runtime, but you might be able to open the file and 
                             * update it using the IO File classes.
                             */

                            // Must edit the file directly
                            System.Xml.XmlDocument xml = new System.Xml.XmlDocument();
                            xml.Load(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
                            System.Xml.XmlNode node;
                            node = xml.SelectSingleNode("configuration/appSettings");

                            // Find and replace on the key string
                            node.InnerXml = node.InnerXml.Replace(appSetting.GetKey(i), newKey);

                            // Write the changes back to the file
                            xml.Save(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);

                            string temp = "A new plaintext password was converted to a SecureString for key '" + keys[0] + "," + keys[1] + "," + keys[2] + "...'";
                            Log4NetLogger.Info(temp);
                        }




                        //ConfigurationManager.AppSettings



                        // Loading all keys into a struct makes JSON reporting/debugging easier
                        ServerSettings serverSettings = new ServerSettings();
                        serverSettings.dbHost = keys[0].ToString();
                        serverSettings.dbPort = keys[1].ToString();
                        serverSettings.dbSID = keys[2].ToString();
                        serverSettings.dbUser = keys[3].ToString();
                        serverSettings.dbPasswd = DecryptString(keys[7].ToString());
                        serverSettings.appOwner = keys[5].ToString();
                        serverSettings.dbSupressionDuration = Convert.ToInt32(keys[6].ToString());




                        // Serialize that hash table into a JSON string for easy insertion into the logs
                        string strJSON = Newtonsoft.Json.JsonConvert.SerializeObject(
                                    serverSettings,
                                    Newtonsoft.Json.Formatting.None,
                                    new Newtonsoft.Json.JsonSerializerSettings { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore });
                        
                        if (bActive)
                        {
                            Log4NetLogger.Info("Parsing Registered Apps9 Instance # " + i.ToString() + ".");
                            Log4NetLogger.Debug("Keys decoded as: " + strJSON);

                            if (IsBackupProcessing(serverSettings))
                            {
                                Log4NetLogger.Info("Skipping Registered Apps9 Instance # " + i.ToString() + ". Item is processing a backup.");
                                Log4NetLogger.Info("Database: SKIPPED");
                                Log4NetLogger.Info("Application: SKIPPED");
                                Log4NetLogger.Info("Application Soap Gateway: SKIPPED");
                                Log4NetLoggerNimsoft.Info(NimsoftLogTemplate.Replace("<Instance_Name>", serverSettings.dbSID).Replace("<Test_Name>", "Database").Replace("<Test_Result>", "SKIPPED").Replace("<Message>", "This instance is processing a backup."));
                                Log4NetLoggerNimsoft.Info(NimsoftLogTemplate.Replace("<Instance_Name>", serverSettings.dbSID).Replace("<Test_Name>", "Application").Replace("<Test_Result>", "SKIPPED").Replace("<Message>", "This instance is processing a backup."));
                                Log4NetLoggerNimsoft.Info(NimsoftLogTemplate.Replace("<Instance_Name>", serverSettings.dbSID).Replace("<Test_Name>", "SOAP_Gateway").Replace("<Test_Result>", "SKIPPED").Replace("<Message>", "This instance is processing a backup."));
                            }
                            else
                            {
                                ApplicationServerSettings applicationServerSettings = new ApplicationServerSettings();

                                if (CheckDatabaseConnection(serverSettings, ref ErrorMessage))
                                {
                                    // Extract URL, Soap Gateway, Username, & Password from Oracle database
                                    applicationServerSettings = GetApplicationSettings(serverSettings);

                                    Log4NetLogger.Info("Database: " + serverSettings.dbSID + " IS AVAILABLE.");
                                    Log4NetLoggerNimsoft.Info(NimsoftLogTemplate.Replace("<Server_Name>", serverSettings.dbHost).Replace("<Instance_Name>", serverSettings.dbSID).Replace("<Test_Name>", "Database").Replace("<Test_Result>", "PASS").Replace("<Message>", "SUCCESS"));
                                }
                                else
                                {
                                    Log4NetLogger.Info("Database: " + serverSettings.dbSID + " is UNavailable.");
                                    Log4NetLogger.Info("Application: SKIPPED");
                                    Log4NetLogger.Info("Application Soap Gateway: SKIPPED");
                                    Log4NetLoggerNimsoft.Info(NimsoftLogTemplate.Replace("<Server_Name>", serverSettings.dbHost).Replace("<Instance_Name>", serverSettings.dbSID).Replace("<Test_Name>", "Database").Replace("<Test_Result>", "FAIL").Replace("<Message>", ErrorMessage.Replace(",",""))); System.Threading.Thread.Sleep(2);
                                    Log4NetLoggerNimsoft.Info(NimsoftLogTemplate.Replace("<Server_Name>", serverSettings.dbHost).Replace("<Instance_Name>", serverSettings.dbSID).Replace("<Test_Name>", "Application").Replace("<Test_Result>", "SKIPPED").Replace("<Message>", "The database is unavailable.")); System.Threading.Thread.Sleep(2);
                                    //Log4NetLoggerNimsoft.Info(NimsoftLogTemplate.Replace("<Instance_Name>", serverSettings.dbSID).Replace("<Test_Name>", "SOAP_Gateway").Replace("<Test_Result>", "SKIPPED").Replace("<Message>", "The database is unavailable.")); System.Threading.Thread.Sleep(2);
                                    //this.AlertEmail("Database Failure: <b>" + serverSettings.dbSID + "</b> is unavailable.", serverSettings);
                                    continue; // Skip Next Checks
                                }

                                if (CheckApplicationServer(applicationServerSettings, ref ErrorMessage))
                                {
                                    Log4NetLogger.Info("Application: " + applicationServerSettings.url + " IS AVAILABLE.");
                                    Log4NetLoggerNimsoft.Info(NimsoftLogTemplate.Replace("<Server_Name>", serverSettings.dbHost).Replace("<Instance_Name>", serverSettings.dbSID).Replace("<Test_Name>", "Application").Replace("<Test_Result>", "PASS").Replace("<Message>", "SUCCESS"));
                                }
                                else
                                {
                                    Log4NetLogger.Info("Application: " + applicationServerSettings.url + " is UNavailable.");
                                    Log4NetLogger.Info("Application Soap Gateway: SKIPPED");

                                    // IFS requested hiding the error message from Nimsoft and ServiceNow Incidents.  (2017-05-10)
                                    //Log4NetLoggerNimsoft.Info(NimsoftLogTemplate.Replace("<Server_Name>", serverSettings.dbHost).Replace("<Instance_Name>", serverSettings.dbSID).Replace("<Test_Name>", "Application").Replace("<Test_Result>", "FAIL").Replace("<Message>", ("The application is unavailable.  Actual Error Message: " + ErrorMessage.Replace(",", "")))); System.Threading.Thread.Sleep(2);
                                    Log4NetLoggerNimsoft.Info(NimsoftLogTemplate.Replace("<Server_Name>", serverSettings.dbHost).Replace("<Instance_Name>", serverSettings.dbSID).Replace("<Test_Name>", "Application").Replace("<Test_Result>", "FAIL").Replace("<Message>", "The application is unavailable.")); System.Threading.Thread.Sleep(2);

                                    //Log4NetLoggerNimsoft.Info(NimsoftLogTemplate.Replace("<Server_Name>", serverSettings.dbHost).Replace("<Instance_Name>", serverSettings.dbSID).Replace("<Test_Name>", "SOAP_Gateway").Replace("<Test_Result>", "SKIPPED").Replace("<Message>", "The application is unavailable.")); System.Threading.Thread.Sleep(2);
                                    // this.AlertEmail("Application Failure: <b>" + url + " </b> is unavailable.", dbSID);
                                    continue; // Skip Next Checks
                                }

                                /* Code Disabled by GDeCecco on 2017-05-10
                                 * 
                                 * FROM: Isuru Wakishta Arachchi 
                                 * SENT: Wednesday, May 10, 2017 3:50 AM
                                 * TEXT: 
                                 * 
                                 * I was the one who conceived the idea of the soapgateway check for the first/legacy version of this tool. 
                                 * But the developer who implemented/tested my idea told me that the soapgteway check passes when DB alone 
                                 * is unavailable (whereas the check should fail if DB is unavailable). This got me thinking and that is 
                                 * when I conceived the idea of the direct DB check and the application check (although they are intrusive). 
                                 * However I didn’t have the soapgaeway check dropped. But I assure you that the soapgateway check is really 
                                 * redundant because we now have both the DB and application checks. Therefore I suggest your developer to 
                                 * either remove the code that perform this redundant check or you simply do not bother about testing 
                                 * soapgateway check. But if you really need to test this then Apache web service stopping and preventing 
                                 * the skipping of soapgateway check manually should see the soapgateway check failing. Recreating a 
                                 * scenario where the sopagateway check fails without at the same time application check failing requires 
                                 * quite a few actions and it’s not worth the effort.
                                 * 
                                 * 
                                if (CheckSoapGateway(applicationServerSettings, ref ErrorMessage))
                                {
                                    Log4NetLogger.Info("Application Soap Gateway: " + applicationServerSettings.soapgateway + " IS AVAILABLE.");
                                    Log4NetLoggerNimsoft.Info(NimsoftLogTemplate.Replace("<Server_Name>", serverSettings.dbHost).Replace("<Instance_Name>", serverSettings.dbSID).Replace("<Test_Name>", "SOAP_Gateway").Replace("<Test_Result>", "PASS").Replace("<Message>", "SUCCESS"));
                                }
                                else
                                {
                                    Log4NetLogger.Info("Application Soap Gateway: " + applicationServerSettings.soapgateway + " is UNavailable.");
                                    Log4NetLoggerNimsoft.Info(NimsoftLogTemplate.Replace("<Server_Name>", serverSettings.dbHost).Replace("<Instance_Name>", serverSettings.dbSID).Replace("<Test_Name>", "SOAP_Gateway").Replace("<Test_Result>", "FAIL").Replace("<Message>", ErrorMessage.Replace(",", "")));
                                    //this.AlertEmail("Application Soap Gateway Failure: <b>" + soapgateway + "</b> is unavailable.", dbSID);
                                    continue; // Skip Next Checks
                                }
                                */
                                ErrorMessage = "";
                            }
                        }
                        else
                        {
                            Log4NetLogger.Info("Skipping Registered Apps9 Instance # " + i.ToString() + ". This instance is disabled in config file.");
                            Log4NetLogger.Debug("Keys decoded as: " + strJSON);
                            Log4NetLogger.Info("Database: SKIPPED");
                            Log4NetLogger.Info("Application: SKIPPED");
                            Log4NetLogger.Info("Application Soap Gateway: SKIPPED");
                            Log4NetLoggerNimsoft.Info(NimsoftLogTemplate.Replace("<Instance_Name>", serverSettings.dbSID).Replace("<Test_Name>", "Database").Replace("<Test_Result>","SKIPPED").Replace("<Message>", "This instance is disabled in config file.")); System.Threading.Thread.Sleep(2);
                            Log4NetLoggerNimsoft.Info(NimsoftLogTemplate.Replace("<Instance_Name>", serverSettings.dbSID).Replace("<Test_Name>", "Application").Replace("<Test_Result>", "SKIPPED").Replace("<Message>", "This instance is disabled in config file.")); System.Threading.Thread.Sleep(2);
                            Log4NetLoggerNimsoft.Info(NimsoftLogTemplate.Replace("<Instance_Name>", serverSettings.dbSID).Replace("<Test_Name>", "SOAP_Gateway").Replace("<Test_Result>", "SKIPPED").Replace("<Message>", "This instance is disabled in config file.")); System.Threading.Thread.Sleep(2);
                        }
                    }

                }
                catch (Exception e)
                {
                    string Message = "Unexpected error while polling.  Error message: " + e.Message.ToString();
                    Log4NetLogger.Error(Message);
                }
            }
            else
            {
                Log4NetLogger.Error("A parameter in the config file is incorrect.");

                // Signal to the parent code that something is wrong
                // It is up to the parent code to monitor and react.
                this.MonitorStatus = false;
            }
            Log4NetLogger.Info("========== Finished Polling Session ==========");
        }

        public bool MonitorStatusHealthy()
        {
            // When a configuration file has a bad value, execution should cease.
            // This function allows the calling code to see if this monitor instance is still healthy.

            // True = Healthy.  False = Unhealthy.
            return MonitorStatus;
        }

        private string SwitchString(string PropertyName, string OldValue, string NewValue)
        {
            if (OldValue != NewValue)
            {
                string Message = "Switching parameter '" + PropertyName + "' from '" + OldValue + "' to '" + NewValue + "'.";
                //Alert(Message);
                Log4NetLogger.Info(Message);
                return NewValue;
            }
            else
            {
                return OldValue;
            }
        }

        private bool ReadConfig()
        {
            // Return True if everything is without error
            // Otherwise, return False

            Log4NetLogger.Debug("Refreshing running configuration: Customer Name and Mail Server Settings");

            string strParameterName  = "";
            var strJSON = "";
            try
            {
                // Reload the config file
                // Log any updated values

                // Re-read the User Settings values
                ConfigurationManager.RefreshSection("userSettings");
                Properties.Settings.Default.Reload();

                // Load each property into a hash table
                Hashtable hashProperties = new Hashtable();
                foreach (SettingsProperty Property in Properties.Settings.Default.Properties)
                {
                    hashProperties.Add(Property.Name, Properties.Settings.Default[Property.Name].ToString());
                }

                // Serialize that hash table into a JSON string for easy insertion into the logs
                strJSON = Newtonsoft.Json.JsonConvert.SerializeObject(
                            hashProperties,
                            Newtonsoft.Json.Formatting.None,
                            new Newtonsoft.Json.JsonSerializerSettings { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore });
                // Now, if something dies during the import (below) a complete snapshot is already captured


                // Import each property.  Log any changes.
                // SwitchString() provides logging when a variable changes value.
                
                // The Variable                      Type Conversion                The Variable Name                      Current Value                            New Value
                customerName                       =                   SwitchString(strParameterName = "customerName",     customerName,                            Properties.Settings.Default.CustomerName);
                emailSettings.mailFrom             =                   SwitchString(strParameterName = "mailFrom",         emailSettings.mailFrom,                  Properties.Settings.Default.MailFrom);
                emailSettings.mailTo               =                   SwitchString(strParameterName = "mailTo",           emailSettings.mailTo,                    Properties.Settings.Default.MailTo);
                emailSettings.mailSubject          =                   SwitchString(strParameterName = "mailSubject",      emailSettings.mailSubject,               Properties.Settings.Default.MailSubject);
                emailSettings.mailServer           =                   SwitchString(strParameterName = "mailServer",       emailSettings.mailServer,                Properties.Settings.Default.MailServer);
                emailSettings.mailServerPort       = Convert.ToInt32(  SwitchString(strParameterName = "mailServerPort",   emailSettings.mailServerPort.ToString(), Properties.Settings.Default.MailServerPort));
                emailSettings.smtpUsername         =                   SwitchString(strParameterName = "smtpUsername",     emailSettings.smtpUsername,              Properties.Settings.Default.smtpUsername);
                emailSettings.smtpPassword         =                   SwitchString(strParameterName = "smtpPassword",     emailSettings.smtpPassword,              Properties.Settings.Default.smtpPassword);
                emailSettings.smtpAuth             = Convert.ToBoolean(SwitchString(strParameterName = "smtpAuth",         emailSettings.smtpAuth.ToString(),       Properties.Settings.Default.smtpAuth));
                intPollEveryXseconds               = Convert.ToInt32(  SwitchString(strParameterName = "PollEveryXseconds",intPollEveryXseconds.ToString(),         Properties.Settings.Default.PollEveryXseconds));

                // Get the log file directory
                string logFileDirectory = GetLog4netLogLocation(Log4NetLogName);
                       logFileDirectory = Path.GetDirectoryName(logFileDirectory);

                // If value has changed, log it
                       logFileDirectory = SwitchString(strParameterName = "logFileDirectory", logFileDirectory, Properties.Settings.Default.LogFileDirectory);
                       
                // Apply value to Log4Net appenders
                if (SetLog4netLogLocation(Log4NetLogName,        logFileDirectory) == false) { throw new Exception("Log File Directory '" + logFileDirectory + "' is invalid." ); }
                if (SetLog4netLogLocation(Log4NetDebugLogName,   logFileDirectory) == false) { throw new Exception("Debug Log File Directory '" + logFileDirectory + "' is invalid."); }
                if (SetLog4netLogLocation(Log4NetNimsoftLogName, logFileDirectory) == false) { throw new Exception("Nimsoft Log File Directory '" + logFileDirectory + "' is invalid."); }

                Log4NetLogger.Debug("Finished refreshing running configuration.");
                return true;
            }
            catch (Exception e)
            {
                Log4NetLogger.Debug("An error occurred while reading parameter '" + strParameterName + "' from the configuration file.  Error Message: " + e.Message.ToString());
                Log4NetLogger.Debug("Snapshot of the configuration file at time of failure:  " + strJSON);
                return false;
            }
        }

        private bool CheckDatabaseConnection(ServerSettings serverSettings, ref string ErrorMessage)
        {
            try
            {
                string oradb = "Data Source=(DESCRIPTION =" +
                        "(ADDRESS = (PROTOCOL = TCP)(HOST = " + serverSettings.dbHost + ")(PORT = " + serverSettings.dbPort + "))" +
                        "(CONNECT_DATA =" +
                        "(SERVER = DEDICATED)" +
                        "(SERVICE_NAME = " + serverSettings.dbSID + ")));" +
                        "User Id=" + serverSettings.dbUser + ";Password=" + ToInsecureString(serverSettings.dbPasswd) + ";";
                OracleConnection conn = new OracleConnection(oradb);

                oradb = "";

                conn.Open();
                string dbVersion = conn.ServerVersion;
                conn.Close();
                conn.Dispose();
                return true;
            }
            catch (Exception e)
            {
                Log4NetLogger.Debug("Database Connection: Failed - " + e.Message.ToString());
                ErrorMessage = e.Message.ToString();
                return false;
            }
        }

        private ApplicationServerSettings GetApplicationSettings(ServerSettings serverSettings)
        {
            ApplicationServerSettings appServerSettings = new ApplicationServerSettings();
            try
            {
                string oradb = "Data Source=(DESCRIPTION =" +
                        "(ADDRESS = (PROTOCOL = TCP)(HOST = " + serverSettings.dbHost + ")(PORT = " + serverSettings.dbPort + "))" +
                        "(CONNECT_DATA =" +
                        "(SERVER = DEDICATED)" +
                        "(SERVICE_NAME = " + serverSettings.dbSID + ")));" +
                        "User Id=" + serverSettings.dbUser + ";Password=" + ToInsecureString(serverSettings.dbPasswd) + ";";
                OracleConnection conn = new OracleConnection(oradb);
                conn.Open();

                oradb = ""; // blank the connection string to remove the password

                string dbVersion = conn.ServerVersion;
                OracleCommand cmd = new OracleCommand("SELECT NAME, VALUE FROM " + serverSettings.appOwner + ".plsqlap_environment_tab", conn);
                OracleDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader[0].ToString() == "USER")
                    {
                        appServerSettings.username = reader[1].ToString();
                    }
                    if (reader[0].ToString() == "PASSWORD")
                    {
                        appServerSettings.password = ToSecureString(reader[1].ToString());
                    }
                    if (reader[0].ToString() == "CONN_STR")
                    {
                        appServerSettings.soapgateway = reader[1].ToString();
                    }
                }
                reader.Close();
                cmd = new OracleCommand("SELECT VALUE FROM " + serverSettings.appOwner + ".fnd_setting_tab WHERE parameter='URL_EXT_SERVER'", conn);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    appServerSettings.url = reader[0].ToString();
                }
                conn.Close();
                conn.Dispose();
                return appServerSettings;
            }
            catch (Exception e)
            {
                Log4NetLogger.Debug("Database Connection: Failed - " + e.Message.ToString());

                appServerSettings.password = null;
                appServerSettings.soapgateway = null;
                appServerSettings.url = null;
                appServerSettings.username = null;
                return appServerSettings;
            }
        }

        private bool CheckSoapGateway(ApplicationServerSettings appServerSettings, ref string ErrorMessage)
        {
            try
            {
                WebRequest request = WebRequest.Create(appServerSettings.soapgateway);
                WebResponse response = request.GetResponse();
                string responseString = (((HttpWebResponse)response).StatusDescription);
                if (responseString.Trim() == "OK")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                // Serialize the Application Server Settings into a JSON string for easy insertion into the logs
                string strJSON = Newtonsoft.Json.JsonConvert.SerializeObject(
                            appServerSettings,
                            Newtonsoft.Json.Formatting.None,
                            new Newtonsoft.Json.JsonSerializerSettings { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore });

                Log4NetLogger.Debug("Error communicating with soap gateway: " + e.Message.ToString());
                Log4NetLogger.Debug("Application Server Settings: " + strJSON);
                ErrorMessage = e.Message.ToString();
                return false;
            }
        }

        private bool CheckApplicationServer(ApplicationServerSettings applicationServerSettings, ref string ErrorMessage)
        {
            try
            {
                bool RetVal = false;
                ExtConnection extConnection = new ExtConnection();
                         extConnection.LoginExtServer   (applicationServerSettings.url, applicationServerSettings.username, ToInsecureString(applicationServerSettings.password));
                RetVal = extConnection.IsLoggedExtServer(applicationServerSettings.url, applicationServerSettings.username, ToInsecureString(applicationServerSettings.password));
                         extConnection.LogoutExtServer  (applicationServerSettings.url, applicationServerSettings.username, ToInsecureString(applicationServerSettings.password));
                return RetVal;
            }
            catch (Exception e)
            {
                // Serialize the Application Server Settings into a JSON string for easy insertion into the logs
                string strJSON = Newtonsoft.Json.JsonConvert.SerializeObject(
                            applicationServerSettings,
                            Newtonsoft.Json.Formatting.None,
                            new Newtonsoft.Json.JsonSerializerSettings { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore });

                Log4NetLogger.Debug("Error communicating with application server: " + e.Message.ToString());
                Log4NetLogger.Debug("Application Server Settings: " + strJSON);
                ErrorMessage = e.Message.ToString();
                return false;
            }
        }
        
        private void AlertEmail(string WhatFailed, ServerSettings serverSettings)
        {
            string msg = "Hi All, <br> Following " + customerName + " <b>" + serverSettings.dbSID + "</b> System is unavailable. Please take immediate actions. <br><br><br> " + DateTime.Now + " - " + WhatFailed;
            
            MailMessage message = new MailMessage(emailSettings.mailFrom, emailSettings.mailTo, emailSettings.mailSubject, msg);
            message.Priority = MailPriority.High;
            message.IsBodyHtml = true;
            SmtpClient client = new SmtpClient(emailSettings.mailServer, emailSettings.mailServerPort);
            if (emailSettings.smtpAuth)
            {
                client.Credentials = new System.Net.NetworkCredential(emailSettings.smtpUsername, emailSettings.smtpPassword);
            }
            //client.UseDefaultCredentials = true;
            client.Send(message);
        }

        /*
        public void Alert(string message)
        {
            string strLogFileFullPath = "";

            // Test the path
            if (Directory.Exists(logFileLocation))
            {
                // The directory exists
                strLogFileFullPath = Path.Combine(logFileLocation, logFileName);

                string msg = DateTime.Now + " - " + message;

                #if (DEBUG)
                    Console.WriteLine(message);
                #endif

                using (StreamWriter w = File.AppendText(strLogFileFullPath))
                {
                    w.Write(msg + Environment.NewLine);
                }

            }
            else
            {
                // The log file path is not valid.
                // Write to the temporary log and exit.
                strLogFileFullPath = Path.Combine(System.IO.Path.GetTempPath(), logFileName);

                // Write the original message
                string msg = DateTime.Now + " - " + message;

                #if (DEBUG)
                    Console.WriteLine(message);
                #endif

                using (StreamWriter w = File.AppendText(strLogFileFullPath))
                {
                    w.Write(msg + Environment.NewLine);
                }

                // Write the error message and exit
                string strErrorMessage = DateTime.Now + " - " + "The path for the log file '" + logFileLocation
                        + "' as specified in the configuration file, is not valid.  The this program will now exit.";

                #if (DEBUG)
                                    Console.WriteLine(strErrorMessage);
                #endif

                using (StreamWriter w = File.AppendText(strLogFileFullPath))
                {
                    w.Write(strErrorMessage + Environment.NewLine);
                }
            }
        }
        */
        private bool IsBackupProcessing(ServerSettings serverSettings)
        {
            // If a backup is active, a text file named after the SID will exist
            string strBackupFilePath = AppDomain.CurrentDomain.BaseDirectory + "Backups\\" + serverSettings.dbSID + ".txt";
            if (File.Exists(strBackupFilePath))
            {
                DateTime creationTime = File.GetCreationTime(strBackupFilePath).AddHours(serverSettings.dbSupressionDuration);
                Log4NetLogger.Debug("Database backup of '" + serverSettings.dbSID + "' has been running since " + creationTime.ToString());

                // If the backup file has existed for less than dbSupressionDuration (hours), then return TRUE
                int result = DateTime.Compare(creationTime, DateTime.Now);
                if (result > 0 )
                {
                    return true;
                }
                else
                {
                    // The backup file has existed for more than dbSupressionDuration (hours).
                    // Pretend the file does not exist.  Return FALSE.
                    Log4NetLogger.Warn("Database backup is in progress, but has been running longer than the dbSupressionDuration of " + serverSettings.dbSupressionDuration.ToString() + " hours.  Please delete the following file: " + strBackupFilePath);
                    //this.Alert("Database backup is taking unusual amount time to complete. : " + serverSettings.dbSID);
                    //this.AlertEmail("Database backup is taking unusual amount time to complete.  <b>" + serverSettings.dbSID + "</b> is unavailable. <br><br> Please Delete if following file is available." + AppDomain.CurrentDomain.BaseDirectory + "Backups\\" + serverSettings.dbSID + ".txt", serverSettings.dbSID);
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private bool SetLog4netLogLocation(string AppenderName, string newLogDiretory)
        {
            // Return true if the appender exists
            // Return false if the appender does not exist
            // Return false if the new directory is invalid

            // Verify the proposed location exists
            if (Directory.Exists(newLogDiretory))
            {
                // Find and update the RollingFileAppender
                foreach (var appender in log4net.LogManager.GetRepository().GetAppenders())
                {
                    if (appender.Name == AppenderName)
                    {
                        // Cast Appender to generic 'FileAppender' type
                        log4net.Appender.FileAppender logger = (log4net.Appender.FileAppender)appender;

                        string oldPath = logger.File;

                        string newPath = Path.Combine(newLogDiretory, Path.GetFileName(oldPath));

                        if (oldPath != newPath)
                        {
                            Log4NetLogger.Info("Logging path is being changed from '" + oldPath + "' to '" + newPath + "'.");

                            logger.File = newPath;
                            logger.ActivateOptions();

                            Log4NetLogger.Info("Logging path has changed from '" + oldPath + "' to '" + newPath + "'.");
                        }
                        return true;  //success
                    }
                }
            }
            else
            {
                Log4NetLogger.Error("Logging path change request to '" + newLogDiretory + "'.  This is an invalid path. Active logging file has not changed.");
            }
            
            return false;
        }

        private string GetLog4netLogLocation(string AppenderName)
        {
            // Find and update the FileAppender
            foreach (var appender in log4net.LogManager.GetRepository().GetAppenders())
            {
                if (appender.Name == AppenderName)
                {
                    // Cast Appender to generic 'FileAppender' type
                    log4net.Appender.FileAppender logger = (log4net.Appender.FileAppender)appender;

                    return logger.File;
                }
            }
            return "";
        }

        public int PollIntervalSeconds ()
        {
            return intPollEveryXseconds;
        }

        private static string EncryptString(System.Security.SecureString input)
        {
            
            byte[] encryptedData = System.Security.Cryptography.ProtectedData.Protect(
                System.Text.Encoding.Unicode.GetBytes(ToInsecureString(input)),
                null,
                System.Security.Cryptography.DataProtectionScope.CurrentUser);
            return Convert.ToBase64String(encryptedData);
        }

        private static SecureString DecryptString(string encryptedData)
        {
            try
            {
                byte[] decryptedData = System.Security.Cryptography.ProtectedData.Unprotect(
                    Convert.FromBase64String(encryptedData),
                    null,
                    System.Security.Cryptography.DataProtectionScope.CurrentUser);
                return ToSecureString(System.Text.Encoding.Unicode.GetString(decryptedData));
            }
            catch
            {
                return new SecureString();
            }
        }

        private static SecureString ToSecureString(string input)
        {
            SecureString secure = new SecureString();
            foreach (char c in input)
            {
                secure.AppendChar(c);
            }
            secure.MakeReadOnly();
            return secure;
        }

        private static string ToInsecureString(SecureString input)
        {
            string returnValue = string.Empty;
            IntPtr ptr = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(input);
            try
            {
                returnValue = System.Runtime.InteropServices.Marshal.PtrToStringBSTR(ptr);
            }
            finally
            {
                System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(ptr);
            }
            return returnValue;
        }
    }
}
