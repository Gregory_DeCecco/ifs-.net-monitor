﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.IO;

namespace IFSApplicationMonitor
{
    public partial class Service1 : ServiceBase
    {
        private Thread m_oPollingThread = null;
        Monitor objMonitor = new Monitor();

        // Initialize Log4Net Logger
        private static readonly log4net.ILog Log4NetLogger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Service1()
        {
            // Set up the Event Log writer
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            eventLog1.WriteEntry("MonitoringService is starting.");
            if (m_oPollingThread == null)
            {
                m_oPollingThread = new Thread(new ThreadStart(StartMonitoring));
            }
            m_oPollingThread.Start();
        }

        protected override void OnStop()
        {
#if (DEBUG)
            Log4NetLogger.Info("Application will now exit.");
            ExitApplication();
#else
            Log4NetLogger.Info("MonitoringService is stopping.");
            eventLog1.WriteEntry("MonitoringService is stopping.");
            m_oPollingThread.Abort();
#endif

        }

        private void ExitApplication()
        {
#if (DEBUG)
            if (System.Windows.Forms.Application.MessageLoop)
            {
                // WinForms app
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                // Console app
                System.Environment.Exit(1);
            }
#endif
        }

        public void StartMonitoring()
        {
            bool Loop = true;

            Log4NetLogger.Info("StartMonitoring()");
            
            do
            {
                try
                {
                    objMonitor.Poll();
                    if (objMonitor.MonitorStatusHealthy())
                    {
                        // Monitor is healthy

                        // Calculate how long to wait for the next "Xminute" period
                        int RunEveryXseconds = objMonitor.PollIntervalSeconds(); 
                        long RemainderTicks = (DateTime.Now.Ticks % (RunEveryXseconds * TimeSpan.TicksPerSecond)); // Divide by RunEveryXseconds, get remainder (Modulo)
                        long SleepTimeTicks = (RunEveryXseconds * TimeSpan.TicksPerSecond) - RemainderTicks;
                        int SleepTimeMilliseconds = (int)(SleepTimeTicks / TimeSpan.TicksPerMillisecond);
                        Thread.Sleep(SleepTimeMilliseconds);
                    }
                    else
                    {
                        // objMonitor.MonitorStatusHealthy() has indicated there is a problem.  Time to exit.
                        Loop = false;
                    }
                }
                catch (Exception e)
                {
#if (DEBUG)
                    Log4NetLogger.Error("An unexpected error ocurred while polling: " + e.Message.ToString());
#else
                    eventLog1.WriteEntry("An unexpected error ocurred while polling: " + e.Message + " - Application will now exit.");
#endif
                    Loop = false;
                }
            }
            while (Loop);

            // Stop the service/application
            this.Stop();
        }
    }
}
