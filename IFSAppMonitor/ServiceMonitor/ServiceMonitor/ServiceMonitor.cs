﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Collections.Specialized;
using System.Net;
using System.IO;
using System.Net.Mail;

namespace ServiceMonitor
{
    public class ServiceMonitor
    {
        NameValueCollection systemUrls;
        string responseString;
        public ServiceMonitor()
        {
            this.ReadConfig();
        }

        public void ReadConfig()
        {
           systemUrls = ConfigurationManager.AppSettings;      
        }
        public void StartMonitoring()
        {
            while (true)
            {
                for (int i = 0; i < systemUrls.Count; i++)
                {
                    if (systemUrls[i].ToString() == "TRUE")
                    {
                        try
                        {
                            WebRequest request = WebRequest.Create(systemUrls.GetKey(i));
                            WebResponse response = request.GetResponse();
                            responseString = (((HttpWebResponse)response).StatusDescription);
                            if (responseString.Trim() != "OK" || responseString.Trim()  != "")
                            {
                                this.Alert(systemUrls.GetKey(i).ToString() + "-" + responseString + "-");
                            }

                        }
                        catch (Exception e)
                        {
                            this.Alert(systemUrls.GetKey(i).ToString() + "-" + responseString + "-");
                        }
                    }
                } 
                
            }

        }

        public void Alert(string systemUrl)
        {
            string msg = systemUrl + " is unavailable.";
            //MailMessage message = new MailMessage("harsha.yapa@ifsworld.com", "harsha.yapa@ifsworld.com", "System Unavailable", msg);
            //SmtpClient client = new SmtpClient("smtp.corpnet.ifsworld.com", 25);
            //client.UseDefaultCredentials = true;
            //client.Send(message);

            using (StreamWriter w = File.AppendText("log.txt"))
            {
                w.Write(msg);
            }
        }

    }
}
