﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.ManagedDataAccess.Client;
using System.Net;
using System.Configuration;
using System.Collections.Specialized;
using System.IO;


namespace ServiceMonitor
{
    public class Monitor
    {
        #region Variables
        string oradb = "";
        NameValueCollection appSetting;
        string[] values;
        string appOwner = "";
        string dbVersion = "";
        string username = "";
        string password = "";
        public string soapgateway = "";
        public string url = "";
        string responseString = "";
        string dbHost = "";
        string dbPort = "";
        string dbSID = "";
        string dbUser = "";
        string dbPasswd = "";
        public bool extServerStatus = false;
        OracleCommand cmd;
        OracleDataReader reader; 
        #endregion

        public void Poll()
        {
            this.ReadConfig();
        }

        public void ReadConfig()
        {
            appSetting = ConfigurationManager.AppSettings;
            for (int i = 0; i < appSetting.Count; i++)
            {
                values = appSetting.GetKey(i).Split(',');
                dbHost = values[0].ToString();
                dbPort = values[1].ToString();
                dbSID = values[2].ToString();
                dbUser = values[3].ToString();
                dbPasswd = values[4].ToString();
                appOwner = values[5].ToString();

                this.CheckDatabaseConnection();
                if (extServerStatus)
                {
                    this.Alert(dbHost + " : " + dbSID);
                }
                this.CheckApplicationServer();
                if (extServerStatus)
                {
                    this.Alert(url);
                }
                this.CheckSoapGateway();
                if (extServerStatus)
                {
                    this.Alert(soapgateway);
                }
            }
        }
        
        private void CheckDatabaseConnection()
        {
            try
            {
                oradb = "Data Source=(DESCRIPTION =" +
                        "(ADDRESS = (PROTOCOL = TCP)(HOST = " + dbHost + ")(PORT = " + dbPort + "))" +
                        "(CONNECT_DATA =" +
                        "(SERVER = DEDICATED)" +
                        "(SERVICE_NAME = " + dbSID + ")));" +
                        "User Id=" + dbUser + ";Password=" + dbPasswd + ";";
                OracleConnection conn = new OracleConnection(oradb);
                conn.Open();
                dbVersion = conn.ServerVersion;
                cmd = new OracleCommand("SELECT NAME, VALUE FROM " + appOwner + ".plsqlap_environment_tab", conn);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    if (reader[0].ToString() == "USER")
                    {
                        username = reader[1].ToString();
                    }
                    if (reader[0].ToString() == "PASSWORD")
                    {
                        password = reader[1].ToString();
                    }
                    if (reader[0].ToString() == "CONN_STR")
                    {
                        soapgateway = reader[1].ToString();
                    }
                }
                reader.Close();
                cmd = new OracleCommand("SELECT VALUE FROM " + appOwner + ".fnd_setting_tab WHERE parameter='URL_EXT_SERVER'", conn);
                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    url = reader[0].ToString();
                }

                conn.Close();
                conn.Dispose();
                extServerStatus = true;
            }
            catch (Exception)
            {
                extServerStatus = false;
            }


        }

        private void CheckSoapGateway()
        {
            try
            {
                WebRequest request = WebRequest.Create(soapgateway);
                WebResponse response = request.GetResponse();
                responseString = (((HttpWebResponse)response).StatusDescription);
                if (responseString.Trim() == "OK")
                {
                    extServerStatus = true;
                }
                else
                {
                    extServerStatus = false;
                }
            }
            catch (Exception)
            {
                extServerStatus = false;
            }
        }

        private void CheckApplicationServer()
        {
            ExtConnection extConnection = new ExtConnection();
            extConnection.LoginExtServer(url, username, password);            
            extServerStatus = extConnection.IsLoggedExtServer(url, username, password);
            extServerStatus = true;
        }

        public void Alert(string system)
        {

            string msg = DateTime.Now + " - " + system + " is unavailable.";

            using (StreamWriter w = File.AppendText("C:\\log.txt"))
            {
                w.Write(msg + Environment.NewLine);
            }

        }
    }
}
