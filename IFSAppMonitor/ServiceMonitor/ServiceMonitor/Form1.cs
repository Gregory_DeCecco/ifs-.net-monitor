﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net.Mail;
using System.Configuration;
using TaskScheduler;
using System.IO;

namespace ServiceMonitor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ServiceMonitor obj = new ServiceMonitor();
            obj.ReadConfig();
            obj.StartMonitoring();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string msg = DateTime.Now + " TEST Email is unavailable.";
            MailMessage message = new MailMessage("ifs@unimin.com", "harsha.yapa@ifsworld.com", "System Unavailable", msg);
            SmtpClient client = new SmtpClient("192.168.5.234", 25);
            //client.UseDefaultCredentials = true;
            client.Send(message);

        }

        private void button3_Click(object sender, EventArgs e)
        {
            Monitor obj = new Monitor();
            obj.Poll();
            MessageBox.Show(obj.soapgateway);
            MessageBox.Show(obj.url);
            if (obj.extServerStatus)
            {
                MessageBox.Show("Environment is running");
            }
            else
            {
                MessageBox.Show("Environment is NOT running");
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Properties.Settings.Default.System);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int result = DateTime.Compare(DateTime.Now.AddHours(3), DateTime.Now);
            if (result > 0)
            {
                
            }
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {

        }
    }
}
